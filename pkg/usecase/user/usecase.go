package user

import (
	"context"
	"encoding/json"

	"github.com/kubuskotak/asgard/tracer"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/trace"
)

type User struct {
	Id    int64  `json:"id"`
	Email string `json:"email"`
}

func (i *impl) GetAllUsers(ctx context.Context) (string, error) {
	span := trace.SpanFromContext(ctx)
	defer span.End()
	l := log.Hook(tracer.TraceContextHook(ctx))
	member := []User{}
	client := i.adapter.OssDbPostgres.Client
	rows, err := client.DB().Query("SELECT id, email FROM sys_ms_member")
	if err != nil {
		l.Err(err).Msgf("err", err.Error())
	}
	for rows.Next() {
		u := User{}
		rows.Scan(u.Id, u.Email)
		member = append(member, u)
	}
	s, e := json.Marshal(member)
	if err != nil {
		l.Err(e).Msgf("err", e.Error())
	}
	return string(s), nil
}
