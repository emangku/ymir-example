// Package user is implements component logic.
package user

import (
	"context"
	"fmt"
	"reflect"

	"entgo.io/ent/dialect"

	"ymirpg/pkg/adapters"
	"ymirpg/pkg/persist/ossdbpersist"
	"ymirpg/pkg/usecase"
)

func init() {
	usecase.Register(usecase.Registration{
		Name: "user",
		Inf:  reflect.TypeOf((*T)(nil)).Elem(),
		New: func() any {
			return &impl{}
		},
	})
}

// T is the interface implemented by all user Component implementations.
type T interface {
	GetAllUsers(cxt context.Context) (string, error)
}

type impl struct {
	adapter *adapters.Adapter
}

// Init initializes the execution of a process involved in a user Component usecase.
func (i *impl) Init(adapter *adapters.Adapter) error {
	i.adapter = adapter
	return nil
}

func WithPgPersist() adapters.Option {
	return func(adapter *adapters.Adapter) {
		if adapter.OssDbPostgres == nil {
			panic(fmt.Errorf("%s is not found", "OssDbPostgres"))
		}

		var c = ossdbpersist.Driver(
			ossdbpersist.WithDriver(adapter.OssDbPostgres.Client, dialect.Postgres),
		)
		adapter.OssdbpersistPersist = c
	}
}
