// Package rest is port handler.
package rest

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	pkgRest "github.com/kubuskotak/asgard/rest"
	pkgTracer "github.com/kubuskotak/asgard/tracer"
)

// OssOption is a struct holding the handler options.
type OssOption func(Oss *Oss)

// Oss handler instance data.
type Oss struct {
}

// NewOss creates a new Oss handler instance.
//
//	var OssHandler = rest.NewOss()
//
//	You can pass optional configuration options by passing a Config struct:
//
//	var adaptor = &adapters.Adapter{}
//	var OssHandler = rest.NewOss(rest.WithOssAdapter(adaptor))
func NewOss(opts ...OssOption) *Oss {
	// Create a new handler.
	var handler = &Oss{}

	// Assign handler options.
	for o := range opts {
		var opt = opts[o]
		opt(handler)
	}

	// Return handler.
	return handler
}

// Register is endpoint group for handler.
func (o *Oss) Register(router chi.Router) {
	// PLEASE EDIT THIS EXAMPLE, how to register handler to router
	router.Get("/hello", pkgRest.HandlerAdapter[GetOssRequest](o.GetOss).JSON)
	router.Get("/hello-csv", pkgRest.HandlerAdapter[GetOssCSVRequest](o.GetOssCSV).CSV)
}

// GetOss endpoint func. /** PLEASE EDIT THIS EXAMPLE, return handler response */.
func (o *Oss) GetOss(w http.ResponseWriter, r *http.Request) (GetOssResponse, error) {
	_, span, l := pkgTracer.StartSpanLogTrace(r.Context(), "Oss")
	defer span.End()
	l.Info().Str("Hello", "World").Msg("this")
	return GetOssResponse{
		Message: "Hello everybody",
	}, nil
}

// GetOssCSV endpoint func. /** PLEASE EDIT THIS EXAMPLE, return handler response */.
func (o *Oss) GetOssCSV(w http.ResponseWriter, r *http.Request) (pkgRest.ResponseCSV, error) {
	_, span, l := pkgTracer.StartSpanLogTrace(r.Context(), "OssCSV")
	defer span.End()

	l.Info().Str("Hello", "World").Msg("this")

	rows := make([][]string, 0)
	rows = append(rows, []string{"SO Number", "Nama Warung", "Area", "Fleet Number", "Jarak Warehouse", "Urutan"})
	rows = append(rows, []string{"SO45678", "WPD00011", "Jakarta Selatan", "1", "45.00", "1"})
	rows = append(rows, []string{"SO45645", "WPD001123", "Jakarta Selatan", "1", "43.00", "2"})
	rows = append(rows, []string{"SO45645", "WPD003343", "Jakarta Selatan", "1", "43.00", "3"})
	return pkgRest.ResponseCSV{
		Filename: "warehouse",
		Rows:     rows,
	}, nil
}
