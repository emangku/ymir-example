// Package rest is port handler.
package rest

// GetOssRequest Get a Oss request.  /** PLEASE EDIT THIS EXAMPLE, request handler */.
type GetOssRequest struct {
}

// GetOssResponse Get a Oss response.  /** PLEASE EDIT THIS EXAMPLE, return handler response */.
type GetOssResponse struct {
	Message string
}

// GetOssCSVRequest Get a Oss request.  /** PLEASE EDIT THIS EXAMPLE, request handler */.
type GetOssCSVRequest struct {
}
